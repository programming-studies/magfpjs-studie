const {
  reduce,
  compose,
} = require('./lib');

const log = console.log.bind(console);

// Note the order. `g' is called first, then `f'.
// Composes exactly two functions.
const compose2 = (f, g) => x => f(g(x));

const upcase = s => s.toUpperCase();
const exclaim = s => `${s}!`;
const shout = compose2(exclaim, upcase);
log(shout('hey'));
// → // HEY!


const head = xs => xs[0];
const reverse = reduce((acc, x) => [x].concat(acc), []);

// `reverse' is called first, then `head' is called.
const last = compose2(head, reverse);

// `last' first reverses the array, putting ‘scheme’ at the
// end, then returns the head of the array.
log(last(['javascript', 'lisp', 'scheme']));
// → scheme

//
// Associativity:
//
//   compose(f, compose(g, h)) === compose(compose(f, g), h);
//
// Composition is associative, meaning it doesn't matter how you
// group two of them.
//

const f1 = compose(upcase, compose(head, reverse));
const f2 = compose(compose(upcase, head), reverse);

log(f1(['x', 'y', 'z']));
// → Z
log(f2(['x', 'y', 'z']));
// → Z

const lastUpper = compose(upcase, head, reverse);
const lastUpperExclaimed = compose(exclaim, upcase, head, reverse);

log(lastUpper(['use', 'the', 'force']));
// → FORCE
log(lastUpperExclaimed(['use', 'the', 'force']));
// → FORCE!

