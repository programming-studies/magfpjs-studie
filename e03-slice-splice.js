const log = console.log.bind(console);

const xs = [1, 2, 3, 4, 5];

// `slice 
// Pure.
xs.slice(0, 3);
// →  [1, 2, 3]

xs.slice(0, 3);
// →  [1, 2, 3]

log(xs);
// → [1, 2, 3, 4, 5]
// `xs' has not changed! I admire its purity.

//
// `splice' changes the array _in place_.
//
// Impure.
//
xs.splice(0,3);
// → [1, 2, 3]

xs.splice(0,3);
// → [4, 5]

xs.splice(0,3);
// →  []

