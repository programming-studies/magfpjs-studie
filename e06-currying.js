const log = console.log.bind(console);
const curry = require('./lib').curry;

const makeAdd = x => y => x + y;

const add1 = makeAdd(1);
const add10 = makeAdd(10);

log(typeof add1, typeof add10);

log(add1(5));
// → 6

log(add10(5));
// → 15

/**
 * @param {regex} regex
 * @param {string} str
 * @return {match|fn(str)}
 */
const match = curry((regex, str) => str.match(regex));

/**
 * @param {string} what
 * @param {string} replacement
 * @param {string} str
 * @return {string|fn(str)}
 */
const replace = curry((what, replacement, str) =>
  str.replace(what, replacement));

/**
 * @param {function} f
 * @param {array} xs
 * @return {array|function}
 */
const filter = curry((f, xs) => xs.filter(f));

/*
 * @param {function} f
 * @param {array} xs
 * @return {array|function}
 */
const map = curry((f, xs) => xs.map(f));

// Passing both arguments does not actually curry it.
log(match(/js/g, 'Node.js is nice!'));
// → [ 'js' ]

// Note we pass only one param  this time.
const hasTheForce = match(/the force/ig);
// → x => x.match(/the force/ig')

// Now we pass the other param.
log(hasTheForce('May the force be with you.'));
// → [ 'the force', index: 4, input: 'May the force be with you.'

log(hasTheForce('May the source be with you.'));
// → null


const strings = ['may the force', 'be with you', 'the force is strong with this one']

const hasWordForce = match(/force/i);
// →  x => x.match(/force/ig)

log(filter(hasWordForce, strings));
// [ 'may the force', 'the force is strong with this one' ]

const removeStringsWithoutForce = filter(hasWordForce);
// → xs => xs.filter(x => x.match(/force/ig))

log(removeStringsWithoutForce(strings));
// [ 'may the force', 'the force is strong with this one' ]

const noVowels = replace(/[aeiou]/ig);
// → (replacement, str) => str.replace(/[aeiou]/ig, replacement)

log(noVowels('*')('lorem ipsum'));

//
// What's demonstrated here is the ability to "pre-load" a function with an
// argument or two in order to receive a new function that remembers those
// arguments.
//

