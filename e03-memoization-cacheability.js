const log = console.log.bind(console);

//
// Pure functions can be  cached by input using _memoization_.
//

const memoize = f => {
  const cache = {};

  return (...args) => {
    log('\n%o', cache);
    const argStr = JSON.stringify(args);
    cache[argStr] = cache[argStr] || f(...args);
    return cache[argStr];
  };
};

const memoizedSqr = memoize(x => x * x);
const dbl = x => x * 2;
const memoizedDbl = memoize(dbl);

log(memoizedSqr(2));
log(memoizedSqr(2));

log(memoizedSqr(5));
log(memoizedSqr(5));

log(memoizedDbl(10));
log(memoizedDbl(10));
log(memoizedDbl(10));
log(memoizedDbl(10));

//
// You can make an impure function into a pure one just by
// delaying evaluation.
//
const pureHttpCall = memoize((url, params) => () => $.getJSON(url, params));
//
// `pureHttpCall' _does not_ make an http request. It instead returns
// a function that when called will do so when called.
//
// In the example above, we don't memoize the results of an http request.
// Rather, we memoize the generated function.
//

