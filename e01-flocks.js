class Flock {
  constructor(n) {
    this.seagulls = n;
  }

  conjoin(other) {
    this.seagulls += other.seagulls;
    return this;
  }

  breed(other) {
    this.seagulls = this.seagulls * other.seagulls
    return this;
  }
}

const flockA = new Flock(4);
const flockB = new Flock(2);
const flockC = new Flock(0);
const result = flockA
  .conjoin(flockC)                  // 4 + 0 = 4
  .breed(flockB)                    // 4 * 2 = 8
  .conjoin(flockA.breed(flockB))    // 8 * 2 = 16, 16 + 16 = 32? Is that it‽ No...
  .seagulls;

console.log(result);
// → 32, which is _incorrect_!

//
// Tracking state is _hard_, confusing and error-prone.
//

