// A:
//
// We consider Car objects of the following shape:
//
//   {
//     name: 'Aston Martin One-77',
//     horsepower: 750,
//     dollar_value: 1850000,
//     in_stock: true,
//   }
//
// Use `compose()` to rewrite the function below.


// isLastInStock :: [Car] -> Boolean
// const isLastInStock = (cars) => {
//   const lastCar = last(cars);
//   return prop('in_stock', lastCar);
// };

const isLastInStock = compose(
  prop('in_stock'),
  last,
);



//
// B:
//
// Considering the following function:
//
//   const average = xs => reduce(add, 0, xs) / xs.length;
//
// Use the helper function `average` to refactor `averageDollarValue` as a composition.

// averageDollarValue :: [Car] -> Int
// const averageDollarValue = (cars) => {
//   const dollarValues = map(c => c.dollar_value, cars);
//   return average(dollarValues);
// };

const averageDollarValue = compose(
  average,
  map(prop('dollar_value')),
);

//
// C:
//
// Refactor `fastestCar` using `compose()` and other functions in pointfree-style.
// Hint, the `flip` function may come in handy.
//
//   fastestCar :: [Car] -> String
//   const fastestCar = (cars) => {
//     const sorted = sortBy(car => car.horsepower, cars);
//     const fastest = last(sorted);
//     return concat(fastest.name, ' is the fastest');
//   };
//

const append = flip(concat);

const fastestCar = compose(
  append(' is the fastest'),
  prop('name'),
  last,
  sortBy(car => car.horsepower),
);

