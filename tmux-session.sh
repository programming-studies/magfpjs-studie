#!/usr/bin/env bash

dir="${HOME}/Projects/functional-programming/mostly-adequate"
ses='magfpjs'

if [ ! -d "$dir" ] ; then
    echo "Directory \`$dir\` not found."
    echo "The project “$ses” doesn't seem to be on this machine."
    echo "Bailing out..."
    exit 0
fi

cd "$dir"

tmux -f ~/Projects/dotfiles/.tmux.conf new-session -d -s "$ses" -c "$dir"

tmux rename-window 'vim'

tmux send-keys "vim -c 'colorscheme mytheme1' -c 'AirlineTheme dark_minimal' dev1.js" C-m

tmux new-window -t "${ses}:2" -n 'shell' -c "$dir" \; \
    send-keys 'ls --all' C-m

tmux new-window -t "${ses}:3" -n 'git' -c "$dir" \; \
    send-keys 'git status' C-m

tmux select-window -t "${ses}:1"

tmux -2 attach-session -t "${ses}"

