const {
  identity,
  curry,
  compose,
  head,
  upCase,
  toLowerCase,
  intercalate,
  split,
  map
} = require('./lib');

const log = console.log.bind(console);

// Impure, but helpful.
const trace = curry((tag, x) => {
  log(tag, x);
  return x;
});

const replace = curry((qry, rpl, str) => str.replace(qry, rpl));

// Not pointfree because we mention the data `name'.
const initials = name => name.split(' ').map(compose(toLowerCase, head)).join('. ');
log(initials('Master Yoda'));

const dasherize = compose(
  intercalate('-'),
  map(toLowerCase),
  trace('after split'), // remember, right to left evaulation
  split(' '),
  replace(/\s{2,}/ig, ' '),
);

log(dasherize('John Carmack has Alien blood'));

