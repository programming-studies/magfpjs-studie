const { Map } = require('immutable');

const jobe = Map({ name: 'Jobe', hp: 20, team: 'red' });
const michael = Map({ name: 'Michael', hp: 20, team: 'green' });

const decrHP = player => player.set('hp', player.get('hp') - 1);

const isSameTeam = (player1, player2) =>
  player1.get('team') === player2.get('team');

const punch = (attacker, target) =>
  (isSameTeam(attacker, target) ? target : decrHP(target));

//
// decrementHP, isSameTeam and punch are all pure and therefore referentially
// transparent. We can use a technique called equational reasoning wherein one
// substitutes "equals for equals" to reason about code. It's a bit like
// manually evaluating the code without taking into account the quirks of
// programmatic evaluation.
//
//
// _equatianl reasoning_
// _referential transparency_
//
