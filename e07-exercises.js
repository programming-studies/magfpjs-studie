const log = console.log.bind(console);
const {
  split,
  filter,
  match,
  reduce,
  keepHighest,
} = require('./lib');

//
// Exercises chapter 4.
//

//
// A:
//
// Refactor to remove all arguments by partially applying the function.
//

// words :: String -> [String]
// const words = str => split(' ', str);
const words = split(' ');
log(words('chicken scheme compiler'));
// → ['chicken', 'scheme', 'compiler']


//
// B:
//
// Refactor to remove all arguments by partially applying the functions.
//

// filterQs :: [String] -> [String]
// const filterQs = xs => filter(x => x.match(/q/i), xs);
const filterQs = filter(match(/q/i));
log(filterQs(['quick', 'camels', 'quarry', 'over', 'quails']));
// → ['quick', 'quarry', 'quails'],


//
// C:
//
// Considering the following function:
//
//   const keepHighest = (x, y) => (x >= y ? x : y);
//
// Refactor `max` to not reference any arguments using the helper function
// `keepHighest`.

// max :: [Number] -> Number
// const max = xs => reduce((acc, x) => (x >= acc ? x : acc), -Infinity, xs);

// Returns `-Infinity' if the input is an empty array.
const max = reduce((x, y) => x >= y ? x : y, -Infinity);
log(max([3, -9, 7, 1]));
// → 7
log(max([]));
// → Infinity

