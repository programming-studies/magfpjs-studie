const log = console.log.bind(console);
//
// IMPURE because someone change change the value of `min'.
//
var min = 21;
const checkAge = age => age >= min;
log(checkAge(20));
// → false
min = 19;
log(checkAge(20));
// → true

//
// Note we call `checkAge' twice with the same number `20', and it
// returns different results because some external value/state
// it depends upon has changed.
//

// IMPURE because someone change change the value of `min'.
class Checker {
  constructor (min) {
    this.min = min;
  }

  check (age) {
    return age >= this.min;
  }

  setMin (min) {
    this.min = min;
  }
}

const checker = new Checker(21);
log(checker.check(20));
// → false.
checker.setMin(19);
log(checker.check(20));
// → true

//
// Not we call `check' twice with the same number `20' and it
// returns different results on each invocation! 💩
// That is because `check' depends upon `age' (which is external
// to `check'). When someone changes the value of `age' in the
// class `Checker', the method `check' produces different results.
//

//
// PURE:
//
const checkAge = age => {
  // `min' is local. No one can change it from the outside.
  // Take this you suckers who want to make my code messy!
  const min = 21;
  return age >= min;
}

