const log = console.log.bind(console);
const info = console.info.bind(console);
const warn = console.warn.bind(console);
const err = console.error.bind(console);

const conjoin = (flockX, flockY) => flockX + flockY;
const breed = (flockX, flockY) => flockX * flockY;

const flockA = 4;
const flockB = 2;
const flockC = 0;

const res =
  conjoin(
    breed(
      flockB,
      conjoin(flockA, flockC)
    ),
    breed(flockA, flockB)
  );

info(res);
// → 16, which is correct!


////////////////////////////////////////////////////////////////////////////////
const add = (x, y) => x + y;
const multiply = (x, y) => x * y;

const fa = 4;
const fb = 2;
const fc = 0;
const result =
  add(multiply(fb, add(fa, fc)), multiply(fa, fb));

info(result);
// → 16

const x = 4,
      y = 2,
      z = 0;

// Associative.
info(add(add(x, y), z) === add(x, add(y, z)));

// Commutative.
info(add(x, y) === add(y, x));

// Identity.
info(add(y, 0) === y)
info(multiply(y, 1) === y);

// Distributive.
info(multiply(x, add(y, z)) === add(multiply(x, y), multiply(x, z)));

//
// Let's use these arithmetic laws for great good.
//

// Original line.
info(add(multiply(flockB, add(flockA, flockC)), multiply(flockA, flockB)));
// → 16

// Apply the identity property to remve the extra `add'.
// add(flockA, flockC) === flockA), because flockC is zero.
info(add(multiply(flockB, flockA), multiply(flockA, flockB)));
// → 16

// Apply distributive property to achieve our result.
info(multiply(flockB, add(flockA, flockA)));
// → 16

//
// throughout this book, we'll sprinkle in some category theory, set theory,
// and lambda calculus and write real world examples that achieve the same
// elegant simplicity and results as our flock of seagulls example.
//
// We'll want to represent our specific problem in terms of generic, composable
// bits and then exploit their properties for our own selfish benefit.
//




