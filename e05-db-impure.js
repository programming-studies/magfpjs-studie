//
// IMPURE
//
var signUp = attrs => {
  const user =  saveUser(attrs);
  welcomeUser(user);
};

//
// Is the function above saving users to a DB? Looks like.
// So, `saveUser' has some internal, sneaky thing with DBs?
//

//
// PURE
//
var signUp = (DB, email, attrs) => () => {
  const user = saveUser(DB, attrs);
  welcomeUser(email, user);
};

//
// Much better. `signUp' is now being explicit that it uses a DB,
// and that email is somehow linked with welcoming the user.
// No sneaky business under the hood.
//
// Also, it is pure because it doesn't actually signs users up.
// It returns a function that will do so.
//
